<?php

/**
 * @file
 * Adding support for the Migrate module.
 */

/**
 * Provides a ipaddress FieldHandler for the Migrate module.
 */
class MigrateFieldIpaddressFieldHandler extends MigrateFieldHandler {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->registerTypes(array('field_ipaddress'));
  }

  /**
   * {@inheritdoc}
   */
  public function prepare($entity, array $field_info, array $instance, array $values) {
    $delta = 0;
    foreach ($values as $value) {
      $start0 = &$value['start0'];
      $start1 = &$value['start1'];
      $end0 = &$value['end0'];
      $end1 = &$value['end1'];
      if (isset($start0) && isset($start1) && isset($end0) && isset($end1)) {
        $return[LANGUAGE_NONE][$delta]['ipaddress']['start0'] = $start0;
        $return[LANGUAGE_NONE][$delta]['ipaddress']['start1'] = $start1;
        $return[LANGUAGE_NONE][$delta]['ipaddress']['end0']   = $end0;
        $return[LANGUAGE_NONE][$delta]['ipaddress']['end1']   = $end1;
        $delta++;
      }
    }
    return isset($return) ? $return : NULL;
  }

}
